"""Task 5: Extracting blue Object by color"""
import cv2 as cv
import numpy as np

"""Read an image"""
image = cv.imread('logo.png')

"""Convert from BGR to HSV"""
image_hsv = cv.cvtColor(image,cv.COLOR_BGR2HSV)

"""Get a mask using cv.inRange()"""
lower_blue = np.array([110,255,255]) 
upper_blue = np.array([130,255,255])
mask = cv.inRange(image_hsv, lower_blue, upper_blue)

"""Extract objects using cv.bitwise_and(). As we only
need the mask function, we can keep the first two
parameters the same.
"""
result = cv.bitwise_and(image, image, mask=mask)

"""Display the result"""
cv.imshow('image',image)
cv.imshow('mask',mask)
cv.imshow('result',result)
cv.waitKey(0)
cv.destroyAllWindows()

