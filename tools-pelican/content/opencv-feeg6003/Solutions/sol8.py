""" Task 8: rotate an image with original center, angle 90, scale 1 """
import numpy as np
import cv2 as cv

"""Read an image"""
img = cv.imread('logo.png')

"""Construct parameters"""
rows, cols = img.shape[:2]
M = cv.getRotationMatrix2D((cols/2,rows/2),90,1)

"""Pass M to cv.warpAffine"""
res = cv.warpAffine(img,M,(cols,rows)) 

"""Display"""
cv.imshow('Rotation',res)
cv.waitKey(0)
cv.destroyAllWindows()

