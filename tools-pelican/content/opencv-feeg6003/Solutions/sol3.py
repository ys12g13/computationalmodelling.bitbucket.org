#TASK3- Obtaining information and doing simple modification
import cv2
import numpy as nump

#1-Load an image in color
colorimg = cv2.imread('atlantis.jpg',1)

#2-From the previous image colorimg print the shape to get row, column
# and channel information
print ('Rows || Columns || Channels')
print colorimg.shape 

#3-From the previous image 'colorimg' access to pixel 50x80
pixel = colorimg[50,80]
print ('Blue || Green || Red')
print pixel

#4-Modifying all the channels in one step to [10,10,10] the
# set of pixels from x1,y1->20x20 to x2,y2->30x50.
colorimg[20:30,20:50] = [10,10,10]

#5-Show the modification from the step4.
# Need to define the frame to display.
cv2.imshow('mod',colorimg)
cv2.waitKey(0)
cv2.destroyAllWindows()

#Extra function
# Split colorimg in three channels
b,g,r = cv2.split(colorimg)
img = cv2.merge((b,g,r))
