title: Introduction to Bayesian Inference and PyMC3
authors: James Waters, Yen-Ye Soh
date: 2019-03-14
tags: Bayesian, stats, PyMC3
slug: PyMC3

This workshop aims to introduce the concepts of bayesian modelling within the PyMC3 probabilistic programming framework for Python. We will cover the basics of Bayesian probability and statistics, using comparisons between classic frequentist probability to motivate . . . 

##PyMC3

Probabilistic programming is the name given to languages that specify probability distributions for model parameters and allow quantifiable uncertainty to be introduced into statistical inference. PyMC3 uses Python's context manager (the `with` keyword as used with file operations), in which models can be defined with *stochastic random variable objects*. 

PyMC3 runs on a Theano backend; a Python library that is optimised for CPU/GPU intensive deep-learning implementations. Namely, Theano builds a compute graph of operations before dynamically compiling to C at runtime. The stochastic objects that are defined within the model context and the relationships between them are created in Theano in this way.

Once a model is created the posterior distributions of the parameters can be approximated with Markov Chain Monte Carlo sampling algorithms, for which great development has been made in the context of bayesian modelling. 

Talk briefly about samplers?

Discuss PyStan/Edward (?) etc, pymc3 has "gradient" sampling for discrete variables (improvements etc)

##Bayesian Probability

Bayesian inference have the potential to be applied to many different fields, although the finer details of the statistics can make it difficult in the practical implementation.
The probabilistic concept of the philosophy with which Bayes is associated with is relatively simple to understand. The existing resources for Bayesian statistics are many and their scope extends the content of this workshop; we will cover the fundamental concepts in order to present the usefulness of its application and to provide an insight into the workings of PyMC3.

The Bayes' rule may appear to be a familiar equation. The equation is derived from the manipulation of the conditional probability equations: given that the probability of event A and B occurring is equivalent to that of event B and A occurring we can deduce:

$$
P(A&B) = P(B&A)
$$

$$
P(A|B)P(B) = P(B|A)P(A)
$$

$$
P(A|B) = \frac{P(B|A)P(A)}{P(B)}
$$

Events A and B can occur in an alternate fashion without changing the overall probability of the events occurring. P(A|B) means the probability of event A occurring given that event B has occurred and in Bayesian Statistics it is viewed as a posterior probability; it is the probability of A taking into account the information B. Bayes' rule therefore indicates that probabilities can take into account additional information. This key property is what differentiates itself from standard statistics and frequentist inference, in which a single point estimate is returned, for example the maximum likelihood estimator (MLE).

The four components of the Bayes' rule therefore consists of:

- `P(A)` the prior probability, this describes the initial belief/probability prior to the observation of data B.
- `P(B|A)` the likelihood. After assuming that hyposthesis or event A is true, this is the probability that you would have observed the data B.
- `P(B)` the marginal likelihood or the normalizing constant. This is the probability that you have observed data B, regardless of whether hypothesis A is true or not.
- `P(A|B)` the posterior probability. This describes our certainty that hypothesis A is true, as a probability, given that we have observed data B. The main goal of Bayesian statistics is to calculate this value.

An analytical example where Bayesian inference can be applied is with the famous Monty Hall problem. The problem initially appeared on the gameshow "Let's make a Deal". Contestants were given a three doors, behind two of which were goats and behind the third was a car. The contestant initiates the game by opening a door. The host, Monty Hall, would then open a one of the remaining doors. Monty never opens a door which reveals the car, knowing what was behind each door, the door he opened always contained a goat. The contestant is then presented with two options:

1.) Open the originally selected door (Stick).

2.) Switch to the other unopened door (Switch).

Bearing in mind that the contestant returns home with the object behind the door he/she finally chooses. One can assume it is in their best interest to pick the door containing the car rather than a goat. 

Let us now examine the probabilities for the two options above. Are there any differences between the two and what probability does either options create?

Prior to the contestant choosing a door, there is exactly one third chance that the door they select contains the prized car. Upon Monty opening another door to reveal a goat, it can be deduced that one of the two remaining doors contains the car. It may initially appear that the contestant now has a $$\frac{1}{2}$$ probability of winning regardless of which door they initially chose. This assumes that the contestant's choices are conditional but independent of one another.

In reality, upon choosing any of the three doors, there is an equal probability, $$\frac{1}{3}$$, of choosing the car. Assume that the contestant chooses door A for the following instances. The probability of Monty opening door B given that you chose door A which contains the car `P(Open B|Car@A)` is $$\frac{1}{2}$$, since no other door contains the car. Likewise, the probability of Monty opening door B given that the car is behind door B `P(Open B|Car@B)` is 0 as the rule states Monty never reveals the car. Finally, the probability of Monty opening door B given that the car is hidden behind door C `P(Open B|Car@C)` is 1; Monty can neither open door A which you have chosen or door C which contains the car.

The Bayes' rule can then be implemented to calculate the overall probability as whether the contestant should stick or switch. This is shown in the Bayes' Box below:

$$
P(A|B) = \frac{P(B|A)P(A)}{P(B)}
$$

Hypothesis|Prior|Likelihood|Prior x Likelihood|Posterior Probability
:--|:--|:--|:--|:--|
`H`|`P(A)`|`P(B|A)`|`P(B|A)P(A)`|`P(A|B)`
`H1=Car@A`|$$\frac{1}{3}$$|`P(B|Car@A)=1/2`|$$\frac{1}{3}$$*$$\frac{1}{2}$$=$$\frac{1}{6}$$|$$\frac{1}{6}$$/$$\frac{1}{2}$$=$$\frac{1}{3}$$
`H2=Car@B`|$$\frac{1}{3}$$|`P(B|Car@B)=0`|$$\frac{1}{3}$$*0=0|0/$$\frac{1}{2}$$=0
`H3=Car@C`|$$\frac{1}{3}$$|`P(B|Car@C)=1`|$$\frac{1}{3}$$*1=$$\frac{1}{3}$$|$$\frac{1}{3}$$/$$\frac{1}{2}$$=$$\frac{2}{3}$$
||||$$\frac{1}{6}$$+$$0$$+$$\frac{1}{3}$$=$$\frac{1}{2}$$=`P(B)`|

The posterior probability of $$\frac{2}{3}$$ for switching to door C, as opposed to the prior probabiltiy of $$\frac{1}{3}$$ for sticking with door A, indicates that the contestant will improve their chances if they were to switch their choice of doors. The Prior probabilities are the same for all doors and Monty maintains the rule of having to reveal a goat with his selection, under these conditions the contestant should always choose to switch their chosen door regardless of which door the contestant initially chooses. Following this strategy will improve their chances of picking the sports car from $$\frac{1}{3}$$ to $$\frac{2}{3}$$.

Evidentally, Bayes' theorem allows for the output to be in the form of beliefs/probabilities since the equation only requires probabilities to calculate. Essentially, the theorem describes how to update the probabilities for hypotheses given actual evidence or data. The application of Bayes' theorem ranges from updating risk evaluation in finance to machine learning techniques that determine whether an email is real or a spam.


##Theano
Theano is used to perform the derivative calculations that allow the more sophisticated MC algorithms to perform intelligent sampling of the parameter space. As Theano has been built to build complex "graphs" of computations that performs the linear algebra calculations necessary to implement deep neural networks across multi-dimensional arrays or tensors. This allows for complex models to be the focus of the programming while efficient computation is acheived through optimized compilation. For more info, see link .!.


