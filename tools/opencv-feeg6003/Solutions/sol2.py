# TASK2 - Managing videos
import cv2
import numpy as nump

#1-Capture the video from a video file named: 'video.avi'.
vid = cv2.VideoCapture('video.avi')

#2-fourcc code defined. For ubuntu it is recommended to use XVID.
fourcc = cv2.VideoWriter_fourcc(*'XVID')

#3-Complete the VideoWriter object flags.
# Define the new video with the name: 'newvideo.avi'
# Use recommended frame rate.
#Hint-The video is expected to be written in grayscale (flag 5).
vwo = cv2.VideoWriter('newvideo.avi',fourcc,25,(int(vid.get(3)),int(vid.get(4))),False)

#4-Check if the video has been opened correctly
while(vid.isOpened()):
    #5-Read the video.
    ret, frame = vid.read()

    #6-If the frame is True we can proceed, if False break the while
    if ret:
     #Extra code -
     # Some modifications to the video, in this case flip it and
     # change it to grayscale
     gray = cv2.flip(frame, 0)
     gra = cv2.cvtColor(gray, cv2.COLOR_BGR2GRAY)

     #7-Write the modified video with the frames called 'gra'.
     #Hint-Call VideoWriter to write the modified frames.
     vwo.write(gra)

     #8-Show the original video
     cv2.imshow('GenuineVideo',frame)

     # Do all the stuff every 25 ms
     cv2.waitKey(25)
    else:
    	break

#Provided- Release either the VideoCapture and the VideoWriter object.
vid.release()
vwo.release()
cv2.destroyWindow('GenuineVideo')

#Display the new video with its modifications
show = cv2.VideoCapture('newvideo.avi')

while(show.isOpened()):
	ret1, frame1 = show.read()
	if(ret1):
		cv2.imshow('ModVideo',frame1)
		cv2.waitKey(25)
	else:
		break

show.release()
cv2.destroyAllWindows()
